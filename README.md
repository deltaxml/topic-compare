# Topic Compare sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Summary

This sample compares two DITA topic files in1.dita and in2.dita. Please read the content of the DITA markup result (results/dita-markup.dita) as it is designed to illustrate several of our DITA comparison product's features. For more details see: [Topic Compare documentation](https://docs.deltaxml.com/dita-compare/latest/topic-sample-3801192.html).

## Running the sample via the Ant build script

The sample comparison can be run via an Apache Ant build script using the following commands.

| Command | Actions Performed |
| --- | --- |
|`ant run` | Run all four comparisons. Default ant target. |
|`ant run-dita-markup` | Run the DITA Markup comparison. |
|`ant run-arbortext-tcs` | Run the Arbortext tracked changes comparison. |
|`ant run-framemaker-tcs` | Run the FrameMaker tracked changes comparison. |
|`ant run-oxygen-tcs` | Run the oXygen tracked changes comparison. |
|`ant run-xmetal-tcs` | Run the XMetaL tracked changes comparison. |
|`ant clean` | Remove the generate output. |

The output of these commands are put into the results directory.

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line
The sample can be run directly by issuing the following command

    java -jar ../../deltaxml-dita.jar compare topic in1.dita in2.dita out.dita output-format=dita-markup

Where `dita-markup` format enumeration can be replaced by one of the tracked changes format enumerations.

Notes:
* When running the command from a Windows operating system command shell the forward slashes (`/`) need to be replaced by backslashes (`\`).

To clean up the sample directory, run the following Ant command.

	ant clean